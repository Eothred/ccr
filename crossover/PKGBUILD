# Maintainer: Yngve Inntjore Levinsen <yngveDOTlevinsenATgmail>
# Contributor: Cian Mc Govern <cian@cianmcgovern.com>
# Contributor: Roland Singer <roland@manjaro.org>
# Contributor: TheBenj <thebenj88 *AT* gmail *DOT* com>

pkgname=crossover
pkgver=12.1.0
pkgrel=1
_pkgdebrel=1
pkgdesc="Run Windows Programs on Linux"
arch=('x86_64')
url="http://www.codeweavers.com"
license=('custom')
makedepends=('binutils' 'tar')
install=${pkgname}.install
replaces=('crossover-games' 'crossover-pro' 'crossover-standard')
depends=('python' 'fontconfig' 'desktop-file-utils' 'alsa-lib' 'lib32-alsa-lib'
         'lib32-fontconfig' 'lib32-libxcursor' 'libxxf86dga' 'libxrandr'
         'libxdamage' 'lib32-libxdamage' 'mesa' 'lib32-mesa' 'lib32-glibc'
         'libxcursor' 'lib32-libsm' 'lib32-libxext' 'lib32-zlib' 'lib32-gcc-libs'
         'lib32-libxrandr' 'lib32-libice' 'lib32-util-linux' 'lib32-e2fsprogs'
         'pygtk' 'lib32-lcms' 'lib32-libpng' 'lib32-libjpeg' 'lib32-libcups')
optdepends=('lib32-nvidia-utils: enables 3D under nvidia cards'
            'lib32-catalyst-utils: enables 3D under ati cards'
            'lib32-libxcursor: coloured mouse pointer support'
            'lib32-libxinerama: enables spanning multiple screens'
            'lib32-openssl:  support for secure Internet communication'
            'lib32-libxxf86vm: perform gamma adjustments'
            'lib32-libxi: enables joystick and tablet support'
            'unzip: required to install Guild Wars, automatic installer extraction'
            )
source=("http://media.codeweavers.com/pub/${pkgname}/cxlinux/demo/ia32-${pkgname}_${pkgver}-1_amd64.deb"
        "cxoffice.conf")
sha256sums=('be4428c81cbedbe23ca967ecd207595104440cda7547b08a1aa5a6b819c3805c'
            '8b3b1d0d996ed904ea5c161b446b2ea5ee2195a8cf79e9f7732e57aa99a83a01')




package() {
    cd $srcdir/


    if [ $CARCH = 'i686' ] ; then
        ar -p crossover_${pkgver}-${_pkgdebrel}_i386.deb data.tar.gz | tar zxf - -C "${pkgdir}" || return 1
        rm -fr $pkgdir/opt/cxoffice/lib/nsplugin-linux64.so
    else
        ar -p ia32-crossover_${pkgver}-${_pkgdebrel}_amd64.deb data.tar.gz | tar zxf - -C "${pkgdir}" || return 1
    fi

    rm $pkgdir/opt/cxoffice/doc # remove symbolic link
    mkdir $pkgdir/opt/cxoffice/doc # create real directory

    mv $pkgdir/usr/share/doc/ia32-crossover/* $pkgdir/opt/cxoffice/doc


    gzip -d $pkgdir/opt/cxoffice/doc/license.txt.gz
    rm $pkgdir/usr -r
    #install -m 644 -D $pkgdir/opt/cxoffice/doc/license.txt $pkgdir/usr/share/licenses/crossover/license
    sed s/\;\;"\"MenuRoot\" = \"\""/"MenuRoot = Windows Games/" -i $pkgdir/opt/cxoffice/share/crossover/bottle_data/cxbottle.conf
    sed s/\;\;"\"MenuStrip\" = \"\""/"MenuStrip = 1/" -i $pkgdir/opt/cxoffice/share/crossover/bottle_data/cxbottle.conf

    #mkdir -p ${pkgdir}/usr/bin
    #ln -s /opt/cxoffice/bin/wine ${pkgdir}/usr/bin/crossover

    # Fix Auto update error
    install -m 644 -D "$srcdir/cxoffice.conf" "$pkgdir/opt/cxoffice/etc/cxoffice.conf"
}
