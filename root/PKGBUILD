# Maintainer: Yngve Inntjore Levinsen < yngve dot levinsen at gmail >
# Contributor: Scott Lawrence <bytbox@gmail.com>
# Contributor: Thomas Dziedzic < gostrc at gmail >
# Contributor: Sebastian Voecking <voeck@web.de>

pkgname=root
pkgver=5.34.18
pkgrel=1
pkgdesc='C++ data analysis framework and interpreter from CERN.'
arch=('i686' 'x86_64')
url='http://root.cern.ch'
screenshot="http://root.cern.ch/drupal/sites/default/files/images/RootShower_0.png"
license=('LGPL2.1')
depends=('avahi'
         'desktop-file-utils'
         'ftgl'
         'giflib'
         'glew'
         'graphviz'
         'gsl'
         'libldap'
         'libmysqlclient'
         'libxft'
         'postgresql-libs'
         'python2'
         'unixodbc'
         'shared-mime-info'
         'xmlrpc-c'
         'xorg-fonts-75dpi'
         'gcc-fortran'
         'hicolor-icon-theme'
         'libiodbc'
         'gtk-update-icon-cache'
         'libafterimage')
makedepends=('fftw' 'cmake')
install='root.install'
options=('!emptydirs' '!makeflags')
source=("ftp://root.cern.ch/root/root_v${pkgver}.source.tar.gz"
        'glibc2_16.patch'
        'no_builtin_ftgl.patch'
        'root.sh'
        'rootd'
        'root.xml')

md5sums=('2a9ef6a342833cb461020fa0832e78fe'
         'f1fdb4f4d2103a71425fe93ba49b4be5'
         'ff7b254b93c767a42531e1750d47d0ce'
         '0e883ad44f99da9bc7c23bc102800b62'
         'efd06bfa230cc2194b38e0c8939e72af'
         'e2cf69b204192b5889ceb5b4dedc66f7')


prepare() {
  rm -rf "${srcdir}/root-build"
  rm -rf "${srcdir}/build"

  cp -r root root-build
  cd root-build

  # not necessary for Chakra for now.. python==python2
#   msg 'python2 switch'
#   find . -type f -exec sed -e 's_#!/usr/bin/env python_&2_' \
#                            -e 's/python -O/python2 -O/g' \
#                            -e 's/python -c/python2 -c/g' -i {} \;
#   sed \
#     -e 's/python /python2 /' \
#     -i config/genreflex.in config/genreflex-rootcint.in

  #msg 'glibc 2.16 patch'
  #patch -p1 -d${srcdir} -i ${srcdir}/glibc2_16.patch
  #msg 'no_builtin_ftgl patch'
  #patch -p1 -i ${srcdir}/no_builtin_ftgl.patch

  cd ../
  mkdir build
  cd build

  for sys_lib in ftgl freetype glew pcre zlib lzma; do
   use_sys_libs+=" -Dbuiltin_${sys_lib}=OFF"
  done

  CFLAGS='-I/usr/include/freetype2/' \
  CXXFLAGS='-I/usr/include/freetype2/' \
   cmake ../root-build \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -Dgdml=ON \
    -Dgsl_shared=ON \
    -Dminuit2=ON \
    -Dsoversion=ON \
    -Droofit=ON \
    -Dbuiltin_afterimage=OFF \
    $use_sys_libs
}
build() {
  cd build
  make
}

package() {
  cd build

  make DESTDIR=${pkgdir} install

  install -D ${srcdir}/root.sh \
    ${pkgdir}/etc/profile.d/root.sh
  install -D ${srcdir}/rootd \
    ${pkgdir}/etc/rc.d/rootd
  install -D -m644 ${srcdir}/root.xml \
    ${pkgdir}/usr/share/mime/packages/root.xml

  install -D -m644 ${srcdir}/root/build/package/debian/root-system-bin.desktop.in \
    ${pkgdir}/usr/share/applications/root-system-bin.desktop
  # replace @prefix@ with /usr for the desktop
  sed -e 's_@prefix@_/usr_' -i ${pkgdir}/usr/share/applications/root-system-bin.desktop

  install -D -m644 ${srcdir}/root/build/package/debian/root-system-bin.png \
    ${pkgdir}/usr/share/icons/hicolor/48x48/apps/root-system-bin.png

  # use a file that pacman can track instead of adding directly to ld.so.conf
  install -d ${pkgdir}/etc/ld.so.conf.d
  echo '/usr/lib/root' > ${pkgdir}/etc/ld.so.conf.d/root.conf

  # Remove some headers that are not supposed to be installed (creates conflicts):
  # (from README: only needed for AIX when render.h is missing)
  rm -rf "${pkgdir}/usr/include/X11"

  rm -rf "${pkgdir}/etc/root/daemons"
}
