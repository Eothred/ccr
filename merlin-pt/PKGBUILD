# Maintainer: Yngve Inntjore Levinsen <yngve.inntjore.levinsen@cern.ch>


pkgname=merlin-pt
pkgver=4.01
_pkgver=4-01
pkgrel=2
pkgdesc="Parallellized Particle Tracker simulation toolkit for accelerator physics."

makedepends=('root')
optional=('root')

arch=('x86_64' 'i686')
license=('custom: http://geant4.cern.ch/license/') 
url="http://geant4.cern.ch/"


source=("merlin-rel-${pkgver}.tar.gz::https://downloads.sourceforge.net/project/merlin-pt/merlin-rel-${_pkgver}.tar.gz?r=https%3A%2F%2Fsourceforge.net%2Fprojects%2Fmerlin-pt%2F&ts=1326465103&use_mirror=netcologne"
        "my_changes.patch")
md5sums=('e0c29ccc9af17cbee442e91f3b07e839'
         'c7c4888b95dd5e646c72718246ef1928')

build() {

    cd rel-${_pkgver}/

    patch -p1 -i ../my_changes.patch

    cd merlin-unix/

    make install
    
    mkdir -p ${pkgdir}/usr/include/
    cp -r lib ${pkgdir}/usr/

    cd ${srcdir}/rel-${_pkgver}
    cp -r Merlin ${pkgdir}/usr/include/merlin-pt

    mkdir -p ${pkgdir}/usr/share/doc/${pkgname}/
    cp -r MerlinExamples ${pkgdir}/usr/share/doc/${pkgname}/examples
    
    install -D -m 644 Merlin/copyright.txt ${pkgdir}/usr/share/licenses/${pkgname}/copyright.txt
    
    # Removing non-header files in /usr/include:
    cd ${pkgdir}/usr/include/merlin-pt/
    find . -type f -regex ".*\.cpp" -exec rm {} \;
    find . -type f -regex ".*/depend" -exec rm {} \;
    find . -type f -regex ".*/catagory" -exec rm {} \;
    find . -type f -regex ".*/Makefile" -exec rm {} \;
    find . -type f -regex ".*\.txt" -exec rm {} \;
}
